-- 1
CREATE USER rentaluser WITH PASSWORD 'rentalpassword';

GRANT CONNECT ON DATABASE dvdrental2 TO rentaluser;

-- 2
GRANT
SELECT
    ON TABLE customer TO rentaluser;

SELECT
    *
FROM
    customer;

-- 3
CREATE ROLE rental NOLOGIN;

GRANT rental TO rentaluser;

-- 4
GRANT
INSERT
,
UPDATE
    ON TABLE rental TO rental;

INSERT INTO
    rental (
        rental_date,
        inventory_id,
        customer_id,
        return_date,
        staff_id,
        last_update
    )
VALUES
    (
        CURRENT_TIMESTAMP,
        1,
        1,
        CURRENT_TIMESTAMP + INTERVAL '3 days',
        1,
        CURRENT_TIMESTAMP
    );

UPDATE
    rental
SET
    return_date = CURRENT_TIMESTAMP + INTERVAL '5 days'
WHERE
    rental_id = 1;

-- 5
REVOKE
INSERT
    ON TABLE rental
FROM
    rental;

INSERT INTO
    rental (
        rental_date,
        inventory_id,
        customer_id,
        return_date,
        staff_id,
        last_update
    )
VALUES
    (
        CURRENT_TIMESTAMP,
        2,
        2,
        CURRENT_TIMESTAMP + INTERVAL '3 days',
        1,
        CURRENT_TIMESTAMP
    );

-- 6
DO $ $ DECLARE customer_rec RECORD;

role_name TEXT;

rental_view_name TEXT;

payment_view_name TEXT;

BEGIN FOR customer_rec IN
SELECT
    customer_id,
    first_name,
    last_name
FROM
    customer
WHERE
    customer_id IN (
        SELECT
            customer_id
        FROM
            payment
    )
    AND customer_id IN (
        SELECT
            customer_id
        FROM
            rental
    ) LOOP role_name := 'client_' || customer_rec.first_name || '_' || customer_rec.last_name;

EXECUTE 'CREATE ROLE ' || quote_ident (role_name) || ' WITH LOGIN PASSWORD ''password''';

rental_view_name := 'rental_view_' || customer_rec.first_name || '_' || customer_rec.last_name;

EXECUTE 'CREATE VIEW ' || quote_ident (rental_view_name) || ' AS ' || 'SELECT * FROM rental WHERE customer_id = ' || customer_rec.customer_id;

payment_view_name := 'payment_view_' || customer_rec.first_name || '_' || customer_rec.last_name;

EXECUTE 'CREATE VIEW ' || quote_ident (payment_view_name) || ' AS ' || 'SELECT * FROM payment WHERE customer_id = ' || customer_rec.customer_id;

EXECUTE 'GRANT SELECT ON ' || quote_ident (rental_view_name) || ' TO ' || quote_ident (role_name);

EXECUTE 'GRANT SELECT ON ' || quote_ident (payment_view_name) || ' TO ' || quote_ident (role_name);

END LOOP;

END $ $;
